import React from 'react';
import BidTableContainer from './Components/Table/BidTableContainer';
import BidFormContainer from './Components/BidForm/BidFormContainer';
import { Container, Row, Col } from 'react-bootstrap';

const App = (props) => {
  return (
    <Container className="mt-1">
      <Row>
        <Col>
          <h4>Заявка на благотворительный забег:</h4>
          <BidFormContainer />
          <h4 className="mb-1">Таблица участников благотворительного забега</h4>
          <BidTableContainer />
        </Col>
      </Row>
    </Container>
  );
}

export default App;
