export const getData = state => state.users.bids;

export const getBidHead = state => state.users.bidHead;

export const getPageSize = state => state.users.pageSize;

export const getTotalItemsCount = state => state.users.totalItemsCount;