import { getUsersAPI } from "../Utils/api/api";

const SET_BIDS = "SET_BIDS";
const ADD_BID = "ADD_BID";
const SET_TOTAL_ITEMS_COUNT = 'SET_TOTAL_ITEMS_COUNT';

const initialState = {
    bidHead: [],
    bids: [],
    totalItemsCount: 0,
    pageSize: 5,
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_BIDS: {
            return {
                ...state,
                bidHead: action.bidHead,
                bids: action.bids
            }
        }
        case ADD_BID: {
            const bid = {
                id: ++state.totalItemsCount,
                date: action.date,
                name: action.name,
                email: action.email,
                phone: action.phone,
                distance: action.distance,
                payment: action.payment
            }
            return {
                ...state,
                bids: [bid, ...state.bids],
                totalItemsCount: state.totalItemsCount++,
            }
        }
        case SET_TOTAL_ITEMS_COUNT: {
            return { ...state, totalItemsCount: action.totalItemsCount }
        }
        default:
            return state;
    }
}

const setBidActionCreator = (bidHead, bids) => ({ type: SET_BIDS, bidHead, bids })
const addBidActionCreator = (date, name, email, phone, distance, payment) => ({ type: ADD_BID, date, name, email, phone, distance, payment })
const setTotalUsersCountActionCreator = (totalItemsCount) => ({ type: SET_TOTAL_ITEMS_COUNT, totalItemsCount })

export const getBids = () => (dispatch) => {
    const users = getUsersAPI.getUsers();
    const user = users.users[0];
    const bidHead = Object.keys(user);

    dispatch(setBidActionCreator(bidHead, users.users));
    dispatch(setTotalUsersCountActionCreator(users.users.length));
}

export const addBid = (props) => (dispatch) => {
    let { date, name, email, phone, distance, payment } = props;
    dispatch(addBidActionCreator(date, name, email, phone, distance, payment))
}

export default reducer;