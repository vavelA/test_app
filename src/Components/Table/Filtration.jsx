import React from 'react';
import { Button, ButtonGroup } from 'react-bootstrap';

const Filtration = ({ sortMode, setSortMode }) => {
    const sortPayments = () => {
        switch (sortMode) {
            case "paymentAscending": {
                setSortMode("paymentDescending");
                break;
            }
            case "paymentDescending": {
                setSortMode("paymentAscending");
                break;
            }
            default: {
                setSortMode("paymentAscending");
                break;
            }
        }       
    }
    const sortDistances = () => {
        switch (sortMode) {
            case "distanceAscending": {
                setSortMode("distanceDescending");
                break;
            }
            case "distanceDescending": {
                setSortMode("distanceAscending");
                break;
            }
            default: {
                setSortMode("distanceAscending");
                break;
            }
        }
    }
    const sortIds = () => {
        switch (sortMode) {
            case "idAscending": {
                setSortMode("idDescending");
                break;
            }
            case "idDescending": {
                setSortMode("idAscending");
                break;
            }
            default: {
                setSortMode("idAscending");
                break;
            }
        }
    }
    return (       
            <ButtonGroup>
                <Button variant="dark" disabled>Фильтрация:</Button>
                <Button variant="dark" onClick={sortIds}>По дате регистрации</Button>
                <Button variant="dark" onClick={sortPayments}>По сумме взноса</Button>
                <Button variant="dark" onClick={sortDistances}>По дистанции забега</Button>
            </ButtonGroup>       
    )
}

export default Filtration;
