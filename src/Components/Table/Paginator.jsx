import React, { useState } from 'react';

import { Button, Pagination } from 'react-bootstrap';

let Paginator = ({ currentPage, totalItemsCount, onPageChanged, pageSize = 5, portionSize = 3 }) => {

    let pagesCount = Math.ceil(totalItemsCount / pageSize);

    let pages = [];
    for (let i = 1; i <= pagesCount; i++) {
        pages.push(i);
    }

    let portionCount = Math.ceil(pagesCount / portionSize);
    let [portionNumber, setPortionNumber] = useState(1);
    let leftPortionPageNumber = (portionNumber - 1) * portionSize + 1;
    let rightPortionPageNumber = portionNumber * portionSize;

    return <Pagination>
        {portionNumber > 1 && <Button variant="dark" onClick={() => { setPortionNumber(portionNumber - 1) }}>PREV</Button>}

        {pages
            .filter(p => p >= leftPortionPageNumber && p <= rightPortionPageNumber)
            .map(p => {
                return <Pagination.Item
                    active={p === currentPage ? true : false }
                    key={p}
                    onClick={(e) => {
                        onPageChanged(p);
                    }}>{p}</Pagination.Item>
            })}

        {portionCount > portionNumber &&
            <Button variant="dark" onClick={() => { setPortionNumber(portionNumber + 1) }}>NEXT</Button>}
    </Pagination>
}

export default Paginator;