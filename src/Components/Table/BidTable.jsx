import React from 'react';
import { Table, Row, Col } from 'react-bootstrap';

import BidHead from './BidHead';
import Bid from './Bid';
import Paginator from './Paginator';
import Filtration from './Filtration';


const BidTable = ({ bids, bidHead, currentPage, pageSize, onPageChanged, totalItemsCount, sortMode, setSortMode }) => {
    let data;
    switch (sortMode) {
        case "idAscending": {
            data = [...bids].sort((a, b) => a.id > b.id ? 1 : -1);
            break;
        }
        case "idDescending": {
            data = [...bids].sort((a, b) => a.id > b.id ? -1 : 1);
            break;
        }
        case "paymentAscending": {
            data = [...bids].sort((a, b) => a.payment > b.payment ? 1 : -1);
            break;
        }
        case "paymentDescending": {
            data = [...bids].sort((a, b) => a.payment > b.payment ? -1 : 1);
            break;
        }
        case "distanceAscending": {
            data = [...bids].sort((a, b) => a.distance > b.distance ? 1 : -1);
            break;
        }
        case "distanceDescending": {
            data = [...bids].sort((a, b) => a.distance > b.distance ? -1 : 1);
            break;
        }
        default: {
            data = [...bids].sort((a, b) => a.id > b.id ? 1 : -1);
            break;
        }
    }
    const startBid = currentPage === 1 ? 0 : (currentPage - 1) * pageSize;
    const endBid = startBid + pageSize;
    const portionBids = data.length !== 0 ? data.slice(startBid, endBid) : []
    const Bids = portionBids.map(bid => <Bid key={bid.id} id={bid.id} bid={Object.keys(bid).map(key => bid[key])} />)
    return (
        <Col>
            <Row>
                <Filtration sortMode={sortMode} setSortMode={setSortMode} />
                <Table striped bordered hover>
                    <thead>
                        <BidHead bidHead={bidHead} />
                    </thead>
                    <tbody>
                        {Bids}
                    </tbody>
                </Table>
            </Row>
            <Row className="ml-2">
                <Paginator
                    currentPage={currentPage}
                    onPageChanged={onPageChanged}
                    totalItemsCount={totalItemsCount} />
            </Row>
        </Col>
    )
}

export default BidTable;