import React from 'react';

const BidHead = ({bidHead}) => {
    const heads = bidHead ? bidHead.map(head => <th key={head}>{head}</th>) : [];
    return (
        <tr>
            {heads}
        </tr>
    )
}

export default BidHead;