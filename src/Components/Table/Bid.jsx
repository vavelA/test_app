import React from 'react';

const Bid = (props) => {
    const elements = props.bid.map((element, index) => <td key={index}>{element}</td>)
    return (
        <tr>
            {elements}
        </tr>
    )
}

export default Bid;