import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { getData, getTotalItemsCount, getPageSize, getBidHead } from '../../Selectors/selector';
import BidTable from './BidTable';
import { getBids } from '../../Redux/reducer';

const BidTableContainer = ({ bids, bidHead, pageSize, totalItemsCount, getBids }) => {

    const [currentPage, setCurrentPage] = useState(1);

    const [sortMode, setSortMode] = useState("idAscending");

    useEffect(() => {
        getBids(currentPage);
    }, [])

    return (
        <BidTable bids={bids}
            bidHead={bidHead}
            currentPage={currentPage}
            onPageChanged={setCurrentPage}
            pageSize={pageSize}
            totalItemsCount={totalItemsCount}
            sortMode={sortMode}
            setSortMode={setSortMode}
        />
    )
}

const mapStateToProps = state => {
    return {
        bids: getData(state),
        bidHead: getBidHead(state),
        pageSize: getPageSize(state),
        totalItemsCount: getTotalItemsCount(state),
    }
}

export default connect(mapStateToProps, { getBids })(BidTableContainer);