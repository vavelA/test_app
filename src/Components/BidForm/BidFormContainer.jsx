import React from 'react';
import { connect } from 'react-redux';
import { reset, getFormValues } from 'redux-form';

import { getData } from '../../Selectors/selector';
import BidForm from './BidForm';
import { addBid } from '../../Redux/reducer';

const BidFormContainer = (props) => {

    const onSubmit = (formData, dispatch) => {
        console.log(formData);
        props.addBid(formData);
        dispatch(reset('bidform'));
    }

    const disable = (props.values === undefined) ? true
        : !(("name" in props.values) && ("date" in props.values) &&
            ("email" in props.values) && ("distance" in props.values) &&
            ("payment" in props.values) && ("phone" in props.values));

    return (
        <BidForm onSubmit={onSubmit} disable={disable} />
    )
}

const mapStateToProps = state => {
    return {
        data: getData(state),
        values: getFormValues('bidform')(state),
    }
}

export default connect(mapStateToProps, { addBid })(BidFormContainer);