import React from 'react';
import { reduxForm } from 'redux-form';
import { Form, Button, Row, Col } from 'react-bootstrap';

import { required, email, number, phoneNumber, date } from '../../Utils/FormControls/validators';
import { createField, DataInput, DataSelect, ReduxPhone, ReduxDate } from '../../Utils/FormControls/FormsControls';

const BidForm = (props) => {
    return (
        <Form onSubmit={props.handleSubmit} className="mb-2 ml-5 mr-5">
            <Row>
                <Col>{createField("Антонов Пётр Иванович", "name", [required], DataInput, { type: "text", text: "ФИО" })}</Col>
                <Col>{createField("", "date", [required, date], ReduxDate, { className:"form-control", text: "Дата рождения" })}</Col>
                <Col>{createField("Введите email", "email", [required, email], DataInput, { type: "email", text: "Email" })}</Col>
            </Row>
            <Row>
                <Col>{createField("+7 (902) 645 43 28", "phone", [required, phoneNumber], ReduxPhone, { type: "tel", text: "Телефон" })}</Col>
                <Col>{createField("", "distance", [required], DataSelect, { label: "Дистанция, км" })}</Col>
                <Col>{createField("руб.", "payment", [required, number], DataInput, { type: "number", text: "Сумма взноса" })}</Col>
            </Row>
            <Button variant="dark" type="submit" disabled={props.disable}>
                Submit
            </Button>
        </Form>
    )
}

export default reduxForm({ form: 'bidform' })(BidForm);