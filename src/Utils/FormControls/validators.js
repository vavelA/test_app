export const required = value => (value || typeof value === 'number' ? undefined : 'Required')

const maxLength = max => value => value && value.length > max ? `Must be ${max} characters or less` : undefined;

export const maxLengthPhone = maxLength(11);

export const email = value => value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ? 'Invalid email address' : undefined

export const phoneNumber = value => value && !/^(0|[+][1-9][0-9]{10})$/i.test(value) ? 'Invalid phone number, must be 11 digits' : undefined

export const date = value => value && !/^(0|[0-3]?[1-9].[0-1][0-9].[0-9]{4})$/i.test(value) ? 'Incorrect date format' : undefined

export const number = value => value && isNaN(Number(value)) ? 'Must be a number' : undefined