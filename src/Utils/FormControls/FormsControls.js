import React from 'react';
import { Field } from 'redux-form';
import { Form } from 'react-bootstrap';
import InputMask from 'react-input-mask';

import DayPickerInput from 'react-day-picker/DayPickerInput';
import { formatDate, parseDate } from 'react-day-picker/moment';
import 'react-day-picker/lib/style.css';

import css from './FormsControls.module.css';

const FormControl = ({ input, meta, child, ...props }) => {
    const hasError = meta.touched && meta.error;
    return (
        <div className={css.formControl + " " + (hasError ? css.error : "")}>
            <div>
                {props.children}
            </div>
            {hasError && <span>{meta.error}</span>}
        </div>
    )
}

export const DataInput = (props) => {
    const { input, meta, child, text, ...restProps } = props;
    return (
        <FormControl {...props}>
            <Form.Group>
                <Form.Label htmlFor={props.name}>{text}</Form.Label>
                <Form.Control {...input} {...restProps} />
            </Form.Group>
        </FormControl>
    );
}

export const ReduxPhone = (props) => {
    const { input, meta, child, text, ...restProps } = props;
    return (
        <FormControl {...props}>
            <Form.Group>
                <Form.Label htmlFor={props.name}>{text}</Form.Label>
                <InputMask className="form-control" mask="+79999999999" {...input} {...restProps} />
            </Form.Group>
        </FormControl>
    );
}

export const ReduxDate = (props) => {
    const { input, meta, child, text, ...restProps } = props;
    const inputPropsDate = Object.assign(input, restProps)
    return (
        <FormControl {...props}>
            <Form.Group>
                <Form.Label htmlFor={props.name}>{text}</Form.Label>
                <div>
                    <DayPickerInput inputProps={inputPropsDate}
                        formatDate={formatDate}
                        parseDate={parseDate}
                        format="D.MM.YYYY" />
                </div>
            </Form.Group>
        </FormControl>
    );
}

export const DataSelect = (props) => {
    const { input, label, meta, child, ...restProps } = props;
    return (
        <FormControl {...props}>
            <Form.Group>
                <Form.Label>{label}</Form.Label>
                <Form.Control as="select" {...input} {...restProps}>
                    <option>3</option>
                    <option>5</option>
                    <option>10</option>
                </Form.Control>
            </Form.Group>
        </FormControl>
    );
}

export const createField = (placeholder, name, validators, component, props = {}, text = "") => {
    return (
        <div>
            <Field placeholder={placeholder}
                name={name}
                validate={validators}
                component={component}
                {...props}
            /> {text}
        </div>
    )
}